#!/bin/sh
sudo apt-get install sudo apt-get install -y make build-essential libssl-dev zlib1g-dev libbz2-dev \
libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev libncursesw5-dev xz-utils
git clone https://github.com/yyuu/pyenv.git ~/.pyenv

echo '# PYENV' >> ~/.zshrc
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshenv
echo 'export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.zshenv
echo 'eval "$(pyenv init -)"' >> ~/.zshenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.zshenv
echo 'export PYENV_VIRTUALENV_DISABLE_PROMPT=1' >> ~/.zshenv
