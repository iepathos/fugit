#!/bin/sh
# installs powerline fonts
git clone https://github.com/powerline/fonts.git
./fonts/install.sh

# cleanup
rm -rf fonts