#!/bin/sh
# installs nodejs on ubuntu
sudo apt-get update
sudo apt-get install nodejs npm -y

# usually gets a permissions issue on ubuntu, this fixes
sudo chown -R $(whoami) $(npm config get prefix)/{lib/node_modules,bin,share}