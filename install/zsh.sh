#!/bin/sh
# installs zsh on ubuntu
sudo apt-get install build-essential ncurses-dev yodl autoconf -y

wget -q -O - http://sourceforge.net/projects/zsh/files/zsh/5.3.1/zsh-5.3.1.tar.gz/download | tar xvzf -
cd zsh-5.3.1
./Util/preconfig
./configure
make
sudo make install
which zsh | sudo tee -a /etc/shells
sudo chsh -s "$(which zsh)" "${USER}"

# cleanup
cd ..
rm -rf zsh-5.3.1