#!/bin/sh
# installs go gvm (https://github.com/moovweb/gvm)
zsh < <(curl -s -S -L https://raw.githubusercontent.com/moovweb/gvm/master/binscripts/gvm-installer)

echo '# Go Version Manager' >> ~/.zshrc
echo '[[ -s "/home/glen/.gvm/scripts/gvm" ]] && source "/home/glen/.gvm/scripts/gvm"' >> ~/.zshenv
