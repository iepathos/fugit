package main

import (
	"log"
	"os"
	"os/exec"
)

var installs = []string{
	"mercurial.sh",
	"zsh.sh",
	"ohmyzsh.sh",
	"fonts.sh",
	"htop.sh",
	"terminology.sh",
	"sublime.sh",
	"docker.sh",
	"docker-compose.sh",
	"pyenv.sh",
	"gvm.sh",
	"node.sh",
	"bower.sh",
	"rofi.sh",
}

var updates = []string{
	"apt.sh",
	"pyenv.sh",
	"npm.sh",
}

var home = os.Getenv("HOME")
var fugitPath = home + "/.fugit"

func exists(path string) (bool, error) {
	_, err := os.Stat(path)
	if err == nil {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return true, err
}

func installGit() {
	log.Println("Installing git")
	_, err := exec.Command("sudo", "apt-get", "install", "git", "-y").Output()
	if err != nil {
		log.Println(err)
	}
}

func fugitInstallUpdateCheck() {
	exists, err := exists(fugitPath)
	if err != nil {
		log.Println(err)
	}
	if !exists {
		log.Println("Gathering shell scripts")
		_, err = exec.Command("git", "clone", "git@bitbucket.org:iepathos/fugit.git", fugitPath).Output()
		if err != nil {
			log.Println(err)
		}
	} else {
		log.Println("Shell scripts already in ~/.fugit, pulling latest from bitbucket")
		_, err = exec.Command("sh", fugitPath+"/update/fugit.sh").Output()
		if err != nil {
			log.Println(err)
		}
	}
}

func install() {
	installGit()
	fugitInstallUpdateCheck()
	for _, script := range installs {
		log.Println("Executing", "~/.fugit/install/"+script)
		_, err := exec.Command("sh", fugitPath+"/install/"+script).Output()
		if err != nil {
			log.Println(err)
		}
	}
}

func update() {
	fugitInstallUpdateCheck()
	for _, script := range updates {
		log.Println("Executing", "~/.fugit/update/"+script)
		_, err := exec.Command("sh", fugitPath+"/update/"+script).Output()
		if err != nil {
			log.Println(err)
		}
	}
}

func fugitHelp() {
	log.Println("Valid commands are 'fugit install' and 'fugit update'")
}

func main() {
	if len(os.Args) > 1 {
		argsWithoutProg := os.Args[1:]
		if argsWithoutProg[0] == "update" {
			update()
		} else if argsWithoutProg[0] == "install" {
			install()
		} else {
			fugitHelp()
		}
	} else {
		fugitHelp()
	}
}
