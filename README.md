Tempus Fugit
----------


Automatic development machine setup with all my personal preferences

Please feel free to use, fork it, modify it for your personal preferences.  I used to setup my dev machines manually and that was well and good for a while but the more you change to new hardware and switch around multiple locations the more manual setup becomes intolerable.

- linux OS 100%, can be many various flavors though and will still work excellent.  generally work with a lighter ubuntu distribution due to compatibility concerns.  xubuntu is my go to currently on ubuntu 16.04

After installing OS, clone this repo and run fugit binary.



- custom keybinds
- terminology enlightenment terminal
- zsh default with oh my zsh, agnoster theme
- hack font
- a handful of custom aliases, mostly rely on oh my zsh aliases
- sublime text 3
- latest docker, docker-compose
- latest go through gvm
- latest python with pyenv
- latest node, npm, bower
- mercurial
- htop